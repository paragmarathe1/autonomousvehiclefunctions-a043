/**
 * @(#) Steering_System_Function.java
 */

package AV_Functions;

import AV_Functions.Controller.Controller;
import AV_Misc_Data.Collision_Avoidance;
import AV_Functions.parametric.MinClearanceDistance;

public class Steering_System_Function
{
	private Controller avf01;
	
	private Intelligent_Parking_Assist avf04;
	
	private Passenger_OverRide avf05;
	
	private Safety_Monitoring avf09;
	
	private Lane_Departure_Warning avf10;
	
	private Post_Crash_Notification avf11;
	
	private Traffic_Sign_Recognition avf12;
	
	public Integer weight;
	
	public Integer power;
	
	private MinClearanceDistance MCD;
	
	
	public Collision_Avoidance collision_Avoidance;
	
	private void steerComputation( )
	{
		
	}
	
	private void gearComputation( )
	{
		
	}
	
	private void calculatePower( )
	{
		
	}
	
	private void calculateWeight( )
	{
		
	}
	
	
}
